package com.kodality.lycido;

import java.util.HashMap;
import java.util.Map;

import com.nimbusds.oauth2.sdk.client.ClientInformation;

public class InMemoryClientStore implements ClientStore {

  private Map<String, ClientInformation> map = new HashMap<String, ClientInformation>();

  @Override
  public ClientInformation get(String k) {
    return map.get(k);
  }

  @Override
  public void put(String k, ClientInformation v) {
    map.put(k, v);
  }

  @Override
  public void remove(String k) {
    map.remove(k);
  }

}
