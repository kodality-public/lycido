package com.kodality.lycido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
public class UserinfoRoute extends NimbusRoute {

  private TokenStore tokenStore;

  @Override
  public Response handle(HTTPRequest req) throws Exception {
    try {
      UserInfoRequest userInfoRequest = UserInfoRequest.parse(req);
      AccessToken accessToken = userInfoRequest.getAccessToken();
      Pair<AccessToken, UserInfo> pair = tokenStore.get(accessToken.getValue());
      if (pair != null && pair.getRight() != null) {
        pair.getRight().setClaim("scope", pair.getLeft().getScope());
        return new UserInfoSuccessResponse(pair.getRight());
      } else {
        throw new GeneralException(BearerTokenError.INVALID_TOKEN);
      }
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return new UserInfoErrorResponse(e.getErrorObject());
      }
    }
    return null;
  }

  @Override
  @GetMapping("/userinfo")
  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return super.handle(request, response);
  }
}
