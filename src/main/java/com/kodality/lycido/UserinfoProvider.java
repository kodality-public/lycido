package com.kodality.lycido;

import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

public interface UserinfoProvider {
  UserInfo get(ClientID id);
}
