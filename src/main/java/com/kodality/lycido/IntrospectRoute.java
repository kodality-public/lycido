package com.kodality.lycido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.TokenIntrospectionErrorResponse;
import com.nimbusds.oauth2.sdk.TokenIntrospectionRequest;
import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.nimbusds.oauth2.sdk.token.Token;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
public class IntrospectRoute extends NimbusRoute {

  private TokenStore tokenStore;

  @Override
  public Response handle(HTTPRequest req) throws Exception {
    try {
      TokenIntrospectionRequest introspectionRequest = TokenIntrospectionRequest.parse(req);
      Token token = introspectionRequest.getToken();
      Pair<AccessToken, UserInfo> pair = tokenStore.get(token.getValue());
      if (pair != null) {
        UserInfo userInfo = pair.getRight();
        if (pair.getLeft().getScope() != null) {
          userInfo.setClaim("scope", pair.getLeft().getScope());
          userInfo.setClaim("active", true);
        }
        return new TokenIntrospectionSuccessResponse(userInfo.toJSONObject());
      }
      throw new GeneralException(BearerTokenError.INVALID_TOKEN);
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return new TokenIntrospectionErrorResponse(e.getErrorObject());
      }
    }
    return null;
  }

  @Override
  @PostMapping("/introspect")
  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return super.handle(request, response);
  }
}
