package com.kodality.lycido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

public class LycidoController {

  @Autowired
  private UserinfoRoute userinfoRoute;
  
  @Autowired
  private TokenRoute tokenRoute;

  @GetMapping("/userinfo")
  public void userinfo(HttpServletRequest req, HttpServletResponse res) throws Exception {
    userinfoRoute.handle(req, res);
  }
  
  @PostMapping("/token")
  public void token(HttpServletRequest req, HttpServletResponse res) throws Exception {
    tokenRoute.handle(req, res);
  }
}
