package com.kodality.lycido.exp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ErrorResponse;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Request;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.ServletUtils;

public class Nroute {

  public static interface Parser<A extends Request> {
    A parse(HTTPRequest req) throws Exception;
  }

  public static interface Handler<A extends Request> {
    Response handle(A req) throws Exception;
  }

  public static interface ErrorHandler {
    ErrorResponse create(ErrorObject error);
  }

  public static interface Route {
    void handle(HttpServletRequest req, HttpServletResponse res) throws Exception;
  }

  public static <A extends Request> Route route(Handler<A> handler, Parser<A> parser, ErrorHandler errorHandler) {
    return (req, res) -> {
      try {
        A request = parser.parse(ServletUtils.createHTTPRequest(req));
        Response response = handler.handle(request);
        ServletUtils.applyHTTPResponse(response.toHTTPResponse(), res);
      } catch (GeneralException e) {
        if (e.getErrorObject() != null) {
          ServletUtils.applyHTTPResponse(errorHandler.create(e.getErrorObject()).toHTTPResponse(), res);
        } else {
          throw e;
        }
      }
    };
  }

}
