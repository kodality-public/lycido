package com.kodality.lycido.exp;

import java.util.function.Function;

import com.kodality.lycido.NimbusRoute;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ErrorResponse;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Request;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;

public abstract class NimRoute<A extends Request> extends NimbusRoute {
  public static interface Parser<A> {
    A parse(HTTPRequest req) throws Exception;
  }

  abstract Response handle(A req) throws Exception;

  protected Parser<A> parser;
  protected Function<ErrorObject, ErrorResponse> errorHandler;

  @Override
  public Response handle(HTTPRequest request) throws Exception {
    try {
      A req = parser.parse(request);
      return handle(req);
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return errorHandler.apply(e.getErrorObject());
      } else {
        throw e;
      }
    }
  }
}
