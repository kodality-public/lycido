package com.kodality.lycido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LycidoApplication {

  public static void main(String[] args) {
    SpringApplication.run(LycidoApplication.class, args);
  }

  @Bean
  InMemoryClientStore clientStore() {
    return new InMemoryClientStore();
  }

  @Bean
  SimpleInMemoryTokenStore tokenStore() {
    return new SimpleInMemoryTokenStore();
  }

  @Bean
  UserinfoProvider userinfoProvider() {
    return new SimpleUserinfoProvider(clientStore());
  }

  @Bean
  SimpleClientCredentialsSelector clientCredentialsSelector() {
    return new SimpleClientCredentialsSelector(clientStore());
  }
}
