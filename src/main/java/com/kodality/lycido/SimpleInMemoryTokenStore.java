package com.kodality.lycido;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

public class SimpleInMemoryTokenStore implements TokenStore {
  private Map<String, Pair<AccessToken, UserInfo>> map = new HashMap<>();

  @Override
  public void put(AccessToken at, UserInfo userInfo) {
    long now = System.currentTimeMillis() / 1000;
    userInfo.setClaim("iat", "" + now);
    userInfo.setClaim("exp", "" + (now + at.getLifetime()));
    map.put(at.getValue(), Pair.of(at, userInfo));
  }

  @Override
  public Pair<AccessToken, UserInfo> get(String value) {
    Pair<AccessToken, UserInfo> pair = map.get(value);
    if (pair != null) {
      if (expired(pair.getRight())) {
        map.remove(value);
      } else {
        return pair;
      }
    }
    return null;
  }

  private boolean expired(UserInfo userInfo) {
    return Long.valueOf(userInfo.getStringClaim("exp")) < System.currentTimeMillis() / 1000;
  }
}
