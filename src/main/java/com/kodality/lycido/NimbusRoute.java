package com.kodality.lycido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.http.ServletUtils;

public abstract class NimbusRoute {

  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HTTPRequest httpRequest = ServletUtils.createHTTPRequest(request);
    HTTPResponse resp = httpHandle(httpRequest);
    ServletUtils.applyHTTPResponse(resp, response);
    return null;
  }
  
  public HTTPResponse httpHandle(HTTPRequest req) throws Exception {
    return handle(req).toHTTPResponse();
  }

  public com.nimbusds.oauth2.sdk.Response handle(HTTPRequest req) throws Exception {
    return null;
  }
}
