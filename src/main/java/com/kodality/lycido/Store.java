package com.kodality.lycido;

public interface Store<K,V> {
  V get(K k);
  void put(K k, V v);
  void remove(K k);
}
