package com.kodality.lycido;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class ClientDeleteRoute {
  
  ClientStore clientStore;
  
  @DeleteMapping("/register")
  public Object handle(@RequestParam("id") String clientId) throws Exception {
    clientStore.remove(clientId);
    return ResponseEntity.noContent();
  }
  
}
