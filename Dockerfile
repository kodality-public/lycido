FROM openjdk:11-slim
ARG APP_VERSION
ENV APP_VERSION=${APP_VERSION:-latest}
RUN apt-get update && apt-get install curl -y
COPY build/libs/*.jar app.jar
CMD java ${JAVA_OPTS} -jar app.jar